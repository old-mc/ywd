package fyresmodjam.commands;

import java.util.List;

import fyresmodjam.blessings.BlessingUtils;
import fyresmodjam.blessings.Blessing;
import fyresmodjam.handlers.NewPacketHandler;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;

public class CommandCurrentBlessing implements ICommand {

	@Override
	public int compareTo(Object arg0) {
		return 0;
	}

	@Override
	public String getCommandName() {
		return "currentBlessing";
	}

	@Override
	public String getCommandUsage(ICommandSender icommandsender) {
		return "commands.currentBlessing.usage";
	}

	@Override
	public List getCommandAliases() {
		return null;
	}

	@Override
	public void processCommand(ICommandSender icommandsender, String[] astring) {
		if (icommandsender instanceof EntityPlayer) {
			EntityPlayer entityplayer = (EntityPlayer) icommandsender;

			boolean hasBlessing = BlessingUtils.hasBlessing(entityplayer);

			String blessingMsg = "";

			if (hasBlessing) {
				Blessing bless = BlessingUtils.getBlessingInstance(entityplayer);

				blessingMsg = String.format("§eCurrent Blessing - §o%s\n%s", bless.customName(), bless.description());
			} else {
				blessingMsg = "You don't currently have a blessing";
			}

			NewPacketHandler.SEND_MESSAGE.sendToPlayer(entityplayer, blessingMsg);
		}
	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender icommandsender) {
		return true;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender icommandsender, String[] astring) {
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] astring, int i) {
		return false;
	}

	public int getRequiredPermissionLevel() {
		return 0;
	}

}
