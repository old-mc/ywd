package fyresmodjam.handlers;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.FMLIndexedMessageToMessageCodec;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetHandler;
import net.minecraft.network.NetHandlerPlayServer;

public class ChannelHandler extends FMLIndexedMessageToMessageCodec<IPacket> {

	public ChannelHandler() {
		addDiscriminator(0, BasicPacket.class);

	}

	@Override
	public void encodeInto(ChannelHandlerContext ctx, IPacket packet, ByteBuf data) throws Exception {
		packet.writeBytes(data);
	}

	@Override
	public void decodeInto(ChannelHandlerContext ctx, ByteBuf data, IPacket packet) {
		packet.readBytes(data);

		if (packet instanceof BasicPacket) {
			NewPacketHandler.packetTypes[((BasicPacket) packet).type].data = ((BasicPacket) packet).data;
		}

		EntityPlayer player = null;

		switch (FMLCommonHandler.instance().getEffectiveSide()) {

		case CLIENT:

			player = getClientPlayer();

			if (player != null) {
				packet.executeClient(player);
				packet.executeBoth(player);
			}

			break;

		case SERVER:

			INetHandler netHandler = ctx.channel().attr(NetworkRegistry.NET_HANDLER).get();
			player = ((NetHandlerPlayServer) netHandler).playerEntity;

			if (player != null) {
				packet.executeServer(player);
				packet.executeBoth(player);
			}

			break;

		default:
			break;

		}
	}

	@SideOnly(Side.CLIENT)
	public EntityPlayer getClientPlayer() {
		return Minecraft.getMinecraft().thePlayer;
	}
}