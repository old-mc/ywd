package fyresmodjam.handlers;

import cpw.mods.fml.common.network.FMLOutboundHandler;
import cpw.mods.fml.relauncher.Side;
import fyresmodjam.ModjamMod;
import fyresmodjam.blessings.BlessingUtils;
import fyresmodjam.blocks.BlockTrap;
import fyresmodjam.misc.DamageSources;
import fyresmodjam.misc.EntityStatHelper;
import fyresmodjam.misc.ItemStatHelper;
import fyresmodjam.tileentities.TileEntityTrap;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.DamageSource;
import net.minecraft.world.WorldServer;

@SuppressWarnings("rawtypes")
public class NewPacketHandler {

	public static BasicPacket[] packetTypes = new BasicPacket[256];

	public static int[] potionValues = new int[12];
	public static int[] potionDurations = new int[12];

	public static int[][] mushroomColors = new int[13][2];

	public static String currentDisadvantage = null;

	public static String currentTask = null;
	public static int currentTaskID = -1;
	public static int currentTaskAmount = 0;
	public static int progress = 0;
	public static int tasksCompleted = 0;
	public static int rewardLevels = 0;

	public static boolean enderDragonKilled = false;
	public static boolean trapsDisabled = false;

	public static void sendPacketToPlayer(IPacket packet, EntityPlayer player) {
		ModjamMod.channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGET)
				.set(FMLOutboundHandler.OutboundTarget.PLAYER);
		ModjamMod.channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGETARGS).set(player);
		ModjamMod.channels.get(Side.SERVER).writeOutbound(packet);
	}

	public static void sendPacketToAllPlayers(IPacket packet) {
		ModjamMod.channels.get(Side.SERVER).attr(FMLOutboundHandler.FML_MESSAGETARGET)
				.set(FMLOutboundHandler.OutboundTarget.ALL);
		ModjamMod.channels.get(Side.SERVER).writeOutbound(packet);
	}

	public static void sendPacketToServer(IPacket packet) {
		ModjamMod.channels.get(Side.CLIENT).attr(FMLOutboundHandler.FML_MESSAGETARGET)
				.set(FMLOutboundHandler.OutboundTarget.TOSERVER);
		ModjamMod.channels.get(Side.CLIENT).writeOutbound(packet);
	}

	public static final BasicPacket UPDATE_BLESSING = new BasicPacket(1) {
		@Override
		public void executeBoth(EntityPlayer player) {
			player.getEntityData().setString("Blessing", (String) data[0]);
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { String.class };
		}
	};

	public static final BasicPacket PLAY_SOUND = new BasicPacket(2) {
		@Override
		public void executeServer(EntityPlayer player) {
			String sound = (String) data[0];
			int x = (Integer) data[1], y = (Integer) data[2], z = (Integer) data[3];
			player.worldObj.playSound(x, y, z, "fyresmodjam:" + sound, 1.0F, 1.0F, false);
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { String.class, Integer.class, Integer.class, Integer.class };
		}
	};

	public static final BasicPacket UPDATE_POTION_KNOWLEDGE = new BasicPacket(3) {
		@Override
		public void executeBoth(EntityPlayer player) {
			player.getEntityData().setIntArray("PotionKnowledge", (int[]) data[0]);
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { int[].class };
		}
	};

	public static final BasicPacket SEND_MESSAGE = new BasicPacket(4) {
		@Override
		public void executeClient(EntityPlayer player) {
			String style = "";

			for (String s : ((String) data[0]).split("@")) {
				String[] words = s.split(" ");
				s = "";

				for (String word : words) {
					s += style + word + " ";

					while (word.contains("\u00A7")) {
						int firstOccurance = word.indexOf("\u00A7");
						String string = word.substring(firstOccurance, firstOccurance + 2);

						if (style.contains(string)) {
							style = style.replace(string, "");
						}
						style += string;
						if (string.equals("\u00A7r")) {
							style = "";
						}

						word = word.replaceFirst(string, "");
					}
				}

				player.addChatComponentMessage(new ChatComponentText(s));
			}
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { String.class };
		}
	};

	public static final BasicPacket UPDATE_WORLD_DATA = new BasicPacket(5) {
		@Override
		public void executeClient(EntityPlayer player) {
			potionValues = (int[]) data[0];
			potionDurations = (int[]) data[1];
			currentDisadvantage = (String) data[2];
			currentTask = (String) data[3];
			currentTaskID = (Integer) data[4];
			currentTaskAmount = (Integer) data[5];
			progress = (Integer) data[6];
			tasksCompleted = (Integer) data[7];
			enderDragonKilled = (Boolean) data[8];
			trapsDisabled = !((Boolean) data[9]);
			rewardLevels = (Integer) data[10];
			mushroomColors = (int[][]) data[11];
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { int[].class, int[].class, String.class, String.class, Integer.class, Integer.class,
					Integer.class, Integer.class, Boolean.class, Boolean.class, Integer.class, int[][].class };
		}
	};

	public static final BasicPacket UPDATE_PLAYER_ITEMS = new BasicPacket(6) {
		@Override
		public void executeServer(EntityPlayer player) {
			for (Object stack : player.inventory.mainInventory) {
				if (stack == null || !(stack instanceof ItemStack)) {
					continue;
				}

				ItemStatHelper.processItemStack((ItemStack) stack, ModjamMod.r);
			}
		}

		@Override
		public Class[] getExpectedClasses() {
			return null;
		}
	};

	public final static BasicPacket DISARM_TRAP = new BasicPacket(7) {
		@Override
		public void executeServer(EntityPlayer player) {
			int blockX = (Integer) data[0], blockY = (Integer) data[1], blockZ = (Integer) data[2];
			boolean mechanic = (Boolean) data[3];

			boolean isScout = BlessingUtils.hasBlessing(player, "MarkScouting");

			TileEntity te = player.worldObj.getTileEntity(blockX, blockY, blockZ);

			boolean isValidTrap = te == null || !(te instanceof TileEntityTrap);
			boolean yours = isValidTrap ? false : player.getCommandSenderName().equals(((TileEntityTrap) te).placedBy);

			if (yours || (mechanic ? ModjamMod.r.nextInt(4) != 0 : ModjamMod.r.nextInt(4) == 0)) {
				boolean salvage = yours || (mechanic ? ModjamMod.r.nextBoolean() : (ModjamMod.r.nextInt(4) == 0));

				SEND_MESSAGE.sendToPlayer(player,
						"\u00A7e\u00A7o" + (!salvage ? "You disarmed the trap." : "You disarm and salvage the trap."));

				if (salvage) {
					ItemStack spawned = new ItemStack(ModjamMod.itemTrap, 1,
							player.worldObj.getBlockMetadata(blockX, blockY, blockZ) % BlockTrap.trapTypes);

					player.worldObj.spawnEntityInWorld(
							new EntityItem(player.worldObj, blockX + 0.5F, blockY, blockZ + 0.5F, spawned));
				}

				player.worldObj.setBlockToAir(blockX, blockY, blockZ);
			} else {
				int trapType = player.worldObj.getBlockMetadata(blockX, blockY, blockZ);

				if (trapType % BlockTrap.trapTypes == 0) {
					// Spike trap

					player.attackEntityFrom(DamageSources.trap, 4.0F + (isScout ? 1 : 0));

					if (ModjamMod.r.nextInt(16 - (isScout ? 4 : 0)) == 0) {
						// Spike traps have a chance to poison the player
						PotionEffect poisonEff = new PotionEffect(Potion.poison.id, 100 + (isScout ? 25 : 0), 1);

						((EntityLivingBase) player).addPotionEffect(poisonEff);
					}
				} else if (trapType % BlockTrap.trapTypes == 1) {
					// Flame trap

					if (!player.isBurning()) {
						player.setFire(5 + (isScout ? 1 : 0));
					}
				} else if (trapType % BlockTrap.trapTypes == 2) {
					// Smoke trap

					PotionEffect blindEff = new PotionEffect(Potion.blindness.id, 100 + (isScout ? 25 : 0), 1);
					PotionEffect slowEff = new PotionEffect(Potion.moveSlowdown.id, 100 + (isScout ? 25 : 0), 1);

					player.addPotionEffect(blindEff);
					player.addPotionEffect(slowEff);
				}

				player.worldObj.setBlockToAir(blockX, blockY, blockZ);

				SEND_MESSAGE.sendToPlayer(player, "\u00A7c\u00A7oYou failed to disarm the trap.");

				if (CommonTickHandler.worldData.getDisadvantage().equals("Explosive Traps")) {
					player.worldObj.setBlockToAir(blockX, blockY, blockZ);
					player.worldObj.createExplosion(null, blockX + 0.5F, blockY + 0.5F, blockZ + 0.5F, 1.33F, true);
				}

				player.triggerAchievement(ModjamMod.whoops);
			}
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { Integer.class, Integer.class, Integer.class, Boolean.class };
		}
	};

	public final static BasicPacket EXAMINE_MOB = new BasicPacket(8) {
		@Override
		public void executeServer(EntityPlayer player) {
			int dimension = (Integer) data[0];
			int entityID = (Integer) data[1];

			WorldServer server = null;

			for (WorldServer s : MinecraftServer.getServer().worldServers) {
				if (s.provider.dimensionId == dimension) {
					server = s;
					break;
				}
			}

			if (server != null) {
				Entity entity = server.getEntityByID(entityID);

				if (entity != null) {
					boolean hasBlessing = entity.getEntityData().hasKey("Blessing");

					String blessing2 = hasBlessing ? entity.getEntityData().getString("Blessing") : null;

					if (blessing2 != null) {
						SEND_MESSAGE.sendToPlayer(player,
								"\u00A7eYou notice " + entity.getCommandSenderName() + "\u00A7e is using "
										+ BlessingUtils.getBlessingInstance(blessing2).customName() + ".");
					} else {
						SEND_MESSAGE.sendToPlayer(player,
								"\u00A7eThere doesn't seem to be anything special about "
										+ (entity instanceof EntityPlayer ? "" : "this ")
										+ entity.getCommandSenderName() + "\u00A7e.");
					}
				}
			}
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { Integer.class, Integer.class };
		}
	};

	public final static BasicPacket LEVEL_UP = new BasicPacket(9) {
		@Override
		public void executeBoth(EntityPlayer player) {
			player.addExperienceLevel((Integer) data[0]);
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { Integer.class };
		}
	};

	public final static BasicPacket ACTIVATE_BLESSING = new BasicPacket(10) {
		@Override
		public void executeServer(EntityPlayer player) {
			int x = (Integer) data[0], y = (Integer) data[1], z = (Integer) data[2];

			boolean blessingActive = EntityStatHelper.hasStat(player, "BlessingActive")
					? Boolean.parseBoolean(EntityStatHelper.getStat(player, "BlessingActive"))
					: false;

			if (!EntityStatHelper.hasStat(player, "BlessingCooldown")) {
				EntityStatHelper.giveStat(player, "BlessingCooldown", 0);
			}

			long time = (CommonTickHandler.worldData != null
					&& CommonTickHandler.worldData.getDisadvantage().equals("Neverending Rain"))
							? player.worldObj.getTotalWorldTime()
							: player.worldObj.getWorldTime();

			if (EntityStatHelper.getStat(player, "BlessingCooldown").equals("0")) {
				if (!blessingActive) {
					if (BlessingUtils.hasBlessing(player)) {
						if (BlessingUtils.hasBlessing(player, "BlessingBerserker")) {
							if (hasBlessingCounters(player)) {
								blessingActive = true;
								SEND_MESSAGE.sendToPlayer(player, "\u00A7cYou enter berserk mode.");
							} else {
								SEND_MESSAGE.sendToPlayer(player, "\u00A7cYou have no berserk counters.");
							}
						} else if (BlessingUtils.hasBlessing(player, "BlessingMechanic")) {
							TileEntity te = player.worldObj.getTileEntity(x, y, z);

							if (te != null && te instanceof TileEntityTrap) {
								SEND_MESSAGE.sendToPlayer(player, "\u00A7e\u00A7oYou disarm and salvage the trap.");

								player.worldObj.spawnEntityInWorld(new EntityItem(player.worldObj, x + 0.5F, y,
										z + 0.5F, new ItemStack(ModjamMod.itemTrap, 1,
												player.worldObj.getBlockMetadata(x, y, z) % BlockTrap.trapTypes)));
								player.worldObj.setBlockToAir(x, y, z);

								EntityStatHelper.giveStat(player, "BlessingCooldown", 24000 - (time % 24000));
							} else {
								SEND_MESSAGE.sendToPlayer(player, "\u00A7e\u00A7oNo selected trap.");
							}
						}
					}
				} else {
					blessingActive = false;

					if (BlessingUtils.hasBlessing(player)) {
						if (BlessingUtils.hasBlessing(player, "BlessingBerserker")) {
							SEND_MESSAGE.sendToPlayer(player, "\u00A7cYou calm down.");

							EntityStatHelper.giveStat(player, "BlessingCooldown", 1200);
						}
					}

					EntityStatHelper.giveStat(player, "BlessingTimer", 0);
				}
			} else {
				SEND_MESSAGE.sendToPlayer(player, "\u00A7cBlessing is on cooldown. ("
						+ (Integer.parseInt(EntityStatHelper.getStat(player, "BlessingCooldown"))) / 20 + "s)");
			}

			EntityStatHelper.giveStat(player, "BlessingActive", blessingActive);
		}

		private boolean hasBlessingCounters(EntityPlayer player) {
			return EntityStatHelper.hasStat(player, "BlessingCounter")
					&& Integer.parseInt(EntityStatHelper.getStat(player, "BlessingCounter")) > 0;
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { Integer.class, Integer.class, Integer.class };
		}
	};

	public final static BasicPacket UPDATE_STAT = new BasicPacket(11) {
		@Override
		public void executeBoth(EntityPlayer player) {
			EntityStatHelper.giveStat(player, (String) data[0], data[1]);
		}

		@Override
		public Class[] getExpectedClasses() {
			return new Class[] { String.class, String.class };
		}
	};
}