package fyresmodjam;

import java.util.Random;

import fyresmodjam.blessings.BlessingUtils;
import fyresmodjam.misc.ConfigData;
import fyresmodjam.misc.EntityStat;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityCreeper;

public final class EntityLevelStat extends EntityStat {
	public EntityLevelStat(String name, String value) {
		super(name, value);
	}

	@Override
	public Object getNewValue(Random r) {
		int lvl = 1;

		while (true) {
			int chance = 5 * (Math.max(1, lvl / 5));

			if (ModjamMod.r.nextInt(chance) < ConfigData.levelupChance) {
				return lvl;
			}

			lvl += 1;
		}
	}

	@Override
	public String getAlteredEntityName(EntityLiving entity) {
		int level = 1;

		try {
			String entityName = entity.getEntityData().getString(name);

			level = Integer.parseInt(entityName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String levelSuffix = String.format("Level %d ", level);

		String newName = "";

		String entityName = entity.getCommandSenderName();

		if (BlessingUtils.hasDisadvantage("Organized Foes")) {
			String rank = "Unelisted ";

			switch (level) {
			case 2:
				rank = "Private";
				break;
			case 3:
				rank = "Corporal";
				break;
			case 4:
				rank = "Sergeant";
				break;
			case 5:
				rank = "Lieutenant";
				break;
			case 6:
				rank = "Captain";
				break;
			case 7:
				rank = "Major";
				break;
			case 8:
				rank = "Colonel";
				break;
			case 9:
				rank = "General";
				break;
			default:
				if (level > 9) {
					rank = "Field Marshal";
				} else {
					rank = "Unelisted";
				}
			}

			newName = String.format("%s %s", rank, entityName);
		} else {
			String colPrefix = "";

			if (level == 5) {
				colPrefix = "\u00A7c";
			}

			newName = String.format("%s%s, %s", colPrefix, entityName, levelSuffix);
		}

		return newName;
	}

	@Override
	public void modifyEntity(Entity entity) {
		int level = 1;

		try {
			level = Integer.parseInt(entity.getEntityData().getString(name));
		} catch (Exception e) {
			e.printStackTrace();
		}

		int healthGain = calculateMobHealthGain(entity, level);

		if (healthGain != 0) {
			EntityLivingBase livingBase = (EntityLivingBase) entity;

			float newHealth = livingBase.getMaxHealth() + healthGain;

			livingBase.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(newHealth);

			livingBase.setHealth(newHealth);
		}

		if (level >= ConfigData.blessingLevel) {
			blessMob(entity);
		} else if (BlessingUtils.hasDisadvantage("Diabolic") && ModjamMod.r.nextInt(ConfigData.diabolicChance) == 0) {
			blessMob(entity);
		}

		if (BlessingUtils.hasDisadvantage("Organized Mobs")) {
			equipMob((EntityLivingBase) entity, level);
		}
	}

	private void equipMob(EntityLivingBase entity, int level) {
		if (level < 2) {
			return;
		}

		float regionalDifficulty = entity.worldObj.func_147462_b(entity.posX, entity.posY, entity.posZ);

		int adjustedDifficulty = (int) Math.ceil(level + regionalDifficulty);

		if (ModjamMod.r.nextInt(10) == 0 || adjustedDifficulty > 5) {

		}

		// TODO actually implement this
	}

	private int calculateMobHealthGain(Entity entity, int level) {
		float initHealth = ((EntityLivingBase) entity).getMaxHealth();

		double mult = level * ConfigData.healthLevelMult;
		double addHealth = initHealth * mult;

		int flatHealth = level * ConfigData.healthLevelFlat;

		/*
		 * int levelBase = level - 1; float quarteredHealth = ((EntityLivingBase)
		 * entity).getMaxHealth() / 4;
		 * 
		 * float adjustedMaxHealth = quarteredHealth;
		 * 
		 * float bonusLevel5Health;
		 * 
		 * if (level >= 5) { bonusLevel5Health = quarteredHealth;
		 * 
		 * bonusLevel5Health *= Math.max(1, level / 5); } else { bonusLevel5Health = 0;
		 * }
		 */
		
		return (int) ((initHealth + addHealth) + flatHealth);
	}

	private void blessMob(Entity entity) {
		String chosenBlessing = BlessingUtils.getMobBlessing();

		entity.getEntityData().setString("Blessing", chosenBlessing);

		// Make sure that any mob type specific blessings are patched up
		BlessingUtils.getBlessingInstance(chosenBlessing).correctBlessing(entity);

		if (entity instanceof EntityCreeper) {
			// Blessed creepers are auto-empowered
			EntityCreeper creeper = (EntityCreeper) entity;

			creeper.getDataWatcher().updateObject(17, (byte) 1);

			creeper.getEntityData().setBoolean("powered", true);
		}
	}
}