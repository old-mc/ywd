package fyresmodjam.misc;

import java.util.Random;

import fyresmodjam.ModjamMod;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;

public final class ArmorStatTracker extends ItemStat {
	public String[][] prefixesByRank = { { "Old", "Broken", "Worn", "Weak" },
			{ "Average", "Decent", "Modest", "Ordinary" }, { "Polished", "Tough", "Hardened", "Durable" },
			{ "Elite", "Astonishing", "Reinforced", "Resilient" }, { "Godly", "Divine", "Fabled", "Legendary" } };

	public ArmorStatTracker(String name, Object value) {
		super(name, value);
	}

	@Override
	public Object getNewValue(ItemStack stack, Random r) {
		int i = 1;
		for (; i < 5; i++) {
			if (ModjamMod.r.nextInt(10) < 7) {
				break;
			}
		}
		return i;
	}

	@Override
	public void modifyStack(ItemStack stack, Random r) {
		int rank = Integer.parseInt(stack.getTagCompound().getString(name));
		float damageReduction = (rank - 1) + r.nextFloat() * 0.5F;

		ItemStatHelper.giveStat(stack, "DamageReduction", String.format("%.2f", damageReduction));
		ItemStatHelper.addLore(stack, !String.format("%.2f", damageReduction).equals("0.00") ? "\u00A77\u00A7o  "
				+ (damageReduction > 0 ? "+" : "") + String.format("%.2f", damageReduction) + "% damage reduction"
				: null);

		ItemStatHelper.addLore(stack, "\u00A7eRank: " + rank);
	}

	@Override
	public String getAlteredStackName(ItemStack stack, Random r) {
		String[] list = prefixesByRank[Integer.parseInt(stack.getTagCompound().getString(name)) - 1];
		String prefix = list[r.nextInt(list.length)];

		if (prefix.equals("Sharp") && stack.getItem() instanceof ItemBow) {
			prefix = "Long";
		}

		return "\u00A7f" + prefix + " " + stack.getDisplayName();
	}
}