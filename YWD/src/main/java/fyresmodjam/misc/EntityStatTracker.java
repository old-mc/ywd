package fyresmodjam.misc;

import java.util.ArrayList;

public class EntityStatTracker {
	public Class<?>[] classes;

	public boolean instanceAllowed = false;

	public EntityStatTracker(Class<?>[] classes, boolean instancesAllowed) {
		this.classes = classes;
		instanceAllowed = instancesAllowed;
	}

	public EntityStatTracker(Class<?> c, boolean instancesAllowed) {
		this(new Class[] { c }, instancesAllowed);
	}

	public ArrayList<EntityStat> stats = new ArrayList<EntityStat>();

	public void addStat(EntityStat stat) {
		if (!stats.contains(stat)) {
			stats.add(stat);
		}
	}
}