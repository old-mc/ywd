package fyresmodjam.misc;

import java.util.Random;

import fyresmodjam.ModjamMod;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;

public final class WeaponRankStat extends ItemStat {
	/*
	 * @formatter:off
	 */
	public String[][] prefixesByRank = { { "Old", "Dull", "Broken", "Worn" },
			{ "Average", "Decent", "Modest", "Ordinary" }, { "Strong", "Sharp", "Polished", "Refined" },
			{ "Powerful", "Ruthless", "Elite", "Astonishing" }, { "Godly", "Divine", "Fabled", "Legendary" } };

	/*
	 * @formatter:on
	 */
	public WeaponRankStat(String name, Object value) {
		super(name, value);
	}

	@Override
	public Object getNewValue(ItemStack stack, Random r) {
		int i = 1;
		for (; i < 5; i++) {
			if (ModjamMod.r.nextInt(10) < 6) {
				break;
			}
		}
		return i;
	}

	@Override
	public void modifyStack(ItemStack stack, Random r) {
		int rank = Integer.parseInt(stack.getTagCompound().getString(name));
		float bonusDamage = ((float) rank - 1) / 2 + (r.nextInt(rank + 1) * r.nextFloat());

		ItemStatHelper.giveStat(stack, "BonusDamage", String.format("%.2f", bonusDamage));
		ItemStatHelper
				.addLore(stack,
						!String.format("%.2f", bonusDamage).equals("0.00") ? "\u00A77\u00A7o  "
								+ (bonusDamage > 0 ? "+" : "") + String.format("%.2f", bonusDamage) + " bonus damage"
								: null);

		ItemStatHelper.addLore(stack, "\u00A7eRank: " + rank);
	}

	@Override
	public String getAlteredStackName(ItemStack stack, Random r) {
		String[] list = prefixesByRank[Integer.parseInt(stack.getTagCompound().getString(name)) - 1];
		String prefix = list[r.nextInt(list.length)];

		if (prefix.equals("Sharp") && stack.getItem() instanceof ItemBow) {
			prefix = "Long";
		}

		return "\u00A7f" + prefix + " " + stack.getDisplayName();
	}
}