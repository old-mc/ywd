package fyresmodjam.misc;

import java.util.Random;

import net.minecraft.item.ItemStack;

public class ItemStat {
	public String name;
	public String value;

	public ItemStat(String name, Object value) {
		this.name = name;
		this.value = value.toString();
	}

	public Object getNewValue(ItemStack stack, Random r) {
		return value;
	}

	public String getLore(ItemStack stack) {
		return null;
	}

	public String getAlteredStackName(ItemStack stack, Random r) {
		return stack.getDisplayName();
	}

	public void modifyStack(ItemStack stack, Random r) {
	}
}