package fyresmodjam.misc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import fyresmodjam.blessings.Blessing;
import fyresmodjam.blessings.BlessingUtils;
import fyresmodjam.handlers.NewPacketHandler;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import tconstruct.library.tools.ToolCore;

@SuppressWarnings("rawtypes")
public class ItemStatHelper {
	public static HashMap<Class, ArrayList<ItemStatTracker>> statTrackersByClass = new HashMap<Class, ArrayList<ItemStatTracker>>();

	public static ArrayList<ItemStatTracker> genericTrackers = new ArrayList<ItemStatTracker>();

	public static ArrayList<Class> skip = new ArrayList<Class>();

	public static void addStatTracker(ItemStatTracker statTracker) {
		if (statTracker.classes != null) {
			for (Class c : statTracker.classes) {
				if (!statTrackersByClass.containsKey(c)) {
					statTrackersByClass.put(c, new ArrayList<ItemStatTracker>());
				}

				statTrackersByClass.get(c).add(statTracker);
			}
		}

		if (statTracker.instanceAllowed) {
			genericTrackers.add(statTracker);
		}
	}

	public static ItemStack giveStat(ItemStack stack, String name, Object value) {
		if (!stack.hasTagCompound()) {
			stack.setTagCompound(new NBTTagCompound());
		}

		NBTTagCompound data = stack.stackTagCompound;
		data.setString(name, value.toString());
		return stack;
	}

	public static ItemStack setName(ItemStack stack, String name) {
		if (!stack.hasTagCompound()) {
			stack.setTagCompound(new NBTTagCompound());
		}

		if (!stack.getTagCompound().hasKey("display")) {
			stack.getTagCompound().setTag("display", new NBTTagCompound());
		}

		stack.getTagCompound().getCompoundTag("display").setString("Name", name);

		return stack;
	}

	public static ItemStack addLore(ItemStack stack, String lore) {
		if (!stack.hasTagCompound()) {
			stack.setTagCompound(new NBTTagCompound());
		}

		if (!stack.getTagCompound().hasKey("display")) {
			stack.getTagCompound().setTag("display", new NBTTagCompound());
		}

		if (!stack.getTagCompound().getCompoundTag("display").hasKey("Lore")) {
			stack.getTagCompound().getCompoundTag("display").setTag("Lore", new NBTTagList());
		}

		if (lore != null) {
			((NBTTagList) stack.getTagCompound().getCompoundTag("display").getTag("Lore"))
					.appendTag(new NBTTagString(lore));
		}

		return stack;
	}

	public static String getName(ItemStack stack) {
		if (stack.getTagCompound() != null && stack.getTagCompound().hasKey("display")
				&& stack.getTagCompound().getCompoundTag("display").hasKey("Name")) {
			return stack.getTagCompound().getCompoundTag("display").getString("Name");
		}

		return null;
	}

	public static String getStat(ItemStack stack, String name) {
		String s = null;
		if (stack.getTagCompound() != null && stack.getTagCompound().hasKey(name)) {
			s = stack.getTagCompound().getString(name);
		}

		return s;
	}

	public static boolean hasStat(ItemStack stack, String name) {
		if (stack.getTagCompound() != null && stack.getTagCompound().hasKey(name)) {
			return true;
		}

		return false;
	}

	@SubscribeEvent
	public void playerDrops(PlayerDropsEvent event) {
		if (!event.entity.worldObj.isRemote) {
			if (BlessingUtils.hasBlessing(event.entity)) {
				Blessing blesz = BlessingUtils.getBlessingInstance(event.entity);

				blesz.onPlayerDrops(event);
			}

			if (BlessingUtils.hasDisadvantage("Permadeath")) {
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public void livingHurt(LivingHurtEvent event) {
		if (!event.entity.worldObj.isRemote) {
			float damageMultiplier = 1.0F;

			try {
				damageMultiplier = handlePlayerWeakening(event, damageMultiplier);
			} catch (NullPointerException npex) {
				damageMultiplier = 1.0F;
			}

			if (BlessingUtils.hasBlessing(event.entity)) {
				damageMultiplier = BlessingUtils.getBlessingInstance(BlessingUtils.getBlessing(event.entity))
						.onIncomingDamage(event, damageMultiplier);
			}

			if (event.isCanceled()) {
				return;
			}

			if (event.entity instanceof EntityLivingBase) {
				EntityLivingBase entity = (EntityLivingBase) event.entity;

				damageMultiplier = getArmorReduction(damageMultiplier, entity);
			}

			if (event.source != null && event.source.getEntity() != null) {
				if (event.source.getEntity() instanceof EntityLivingBase) {
					handleWeaponDamage(event);
				}

				String mob = EntityStatHelper.getUnalteredName(event.entity);

				if (ConfigData.enableMobKillStats && event.source.getEntity() instanceof EntityPlayer
						&& event.source.getEntity().getEntityData().hasKey("KillStats")
						&& event.source.getEntity().getEntityData().getCompoundTag("KillStats").hasKey(mob)) {
					damageMultiplier = handleMobKnowledgeBonus(event, damageMultiplier, mob);
				}

				String weapon = "misc";
				if (ConfigData.enableWeaponKillStats && event.source.getEntity() instanceof EntityPlayer
						&& event.source.getEntity().getEntityData().hasKey("WeaponStats")
						&& event.source.getEntity().getEntityData().getCompoundTag("WeaponStats").hasKey(weapon)) {
					damageMultiplier = handleWeaponKnowledgeBonus(event, damageMultiplier, weapon);
				}

				if (BlessingUtils.hasBlessing(event.source.getEntity())) {
					damageMultiplier = BlessingUtils
							.getBlessingInstance(BlessingUtils.getBlessing(event.source.getEntity()))
							.onOutgoingDamage(event, damageMultiplier);
				}
			}

			if (event.isCanceled()) {
				return;
			}

			if (event.ammount < event.entityLiving.getHealth()) {
				event.ammount = (float) Math.max(0.5, event.ammount * damageMultiplier);
			}
		}
	}

	private float handleWeaponKnowledgeBonus(LivingHurtEvent event, float damageMultiplier, String weapon) {
		EntityPlayer player = (EntityPlayer) event.source.getEntity();

		if (player.getHeldItem() == null) {
			weapon = "fist";
		} else if (player.getHeldItem().getItem() != null && player.getHeldItem().getItem() instanceof ItemSword
				|| player.getHeldItem().getItem() instanceof ItemBow
				|| player.getHeldItem().getItem() instanceof ItemAxe) {
			weapon = EntityStatHelper.getUnalteredItemName(player.getHeldItem().getItem());
		}

		int kills = event.source.getEntity().getEntityData().getCompoundTag("WeaponStats").getInteger(weapon);

		int last = 0;
		for (int i = 0; i < EntityStatHelper.killCount.length; i++) {
			if (kills >= EntityStatHelper.killCount[i] * 2) {
				last = i;
				continue;
			} else {
				break;
			}
		}

		float pickedDamageBonus = EntityStatHelper.damageBonus[last];

		if (BlessingUtils.hasBlessing(event.source.getEntity())) {
			if (BlessingUtils.hasBlessing(event.source.getEntity())) {
				pickedDamageBonus = BlessingUtils.getBlessingInstance(event.source.getEntity())
						.adjustMobKnowledgeBonus(event, pickedDamageBonus);
			}
		}

		damageMultiplier += pickedDamageBonus;
		return damageMultiplier;
	}

	private float handleMobKnowledgeBonus(LivingHurtEvent event, float damageMultiplier, String mob) {
		int kills = event.source.getEntity().getEntityData().getCompoundTag("KillStats").getInteger(mob);

		int last = 0;
		for (int i = 0; i < EntityStatHelper.killCount.length; i++) {
			if (kills >= EntityStatHelper.killCount[i]) {
				last = i;
				continue;
			} else {
				break;
			}
		}

		float pickedDamageBonus = EntityStatHelper.damageBonus[last];

		if (BlessingUtils.hasBlessing(event.source.getEntity())) {
			if (BlessingUtils.hasBlessing(event.source.getEntity())) {
				pickedDamageBonus = BlessingUtils.getBlessingInstance(event.source.getEntity())
						.adjustMobKnowledgeBonus(event, pickedDamageBonus);
			}
		}

		damageMultiplier += pickedDamageBonus;
		return damageMultiplier;
	}

	private void handleWeaponDamage(LivingHurtEvent event) {
		EntityLivingBase entity = (EntityLivingBase) event.source.getEntity();

		ItemStack held = entity.getEquipmentInSlot(0);

		if (held != null
				&& ((event.source.getDamageType().equals("player") || event.source.getDamageType().equals("mob")
						|| (held.getItem() == Items.bow && event.source.isProjectile())))) {
			String s = getStat(held, "BonusDamage");
			if (s != null) {
				event.ammount += Float.parseFloat(s.trim().replace(",", "."));
			}
		}
	}

	private float getArmorReduction(float damageMultiplier, EntityLivingBase entity) {
		for (int i = 0; i < 4; i++) {
			ItemStack stack = entity.getEquipmentInSlot(i + 1);
			if (stack == null || stack.getTagCompound() == null || !stack.getTagCompound().hasKey("DamageReduction")) {
				continue;
			}

			damageMultiplier -= Float
					.parseFloat(stack.getTagCompound().getString("DamageReduction").trim().replace(",", ".")) * 0.01F;
		}
		return damageMultiplier;
	}

	private float handlePlayerWeakening(LivingHurtEvent event, float damageMultiplier) {
		if (BlessingUtils.hasDisadvantage("Weak")) {
			if (event.source.getDamageType().equals("player")) {
				damageMultiplier -= 0.25F;
			}
		} else if (BlessingUtils.hasDisadvantage("Tougher Mobs")) {
			if (event.entity instanceof EntityMob) {
				damageMultiplier -= 0.25F;
			}
		}
		return damageMultiplier;
	}

	public static ArrayList<ItemStatTracker> temp = new ArrayList<ItemStatTracker>();

	@SuppressWarnings("unchecked")
	public static void processItemStack(ItemStack stack, Random r) {
		if (stack == null) {
			return;
		}

		for (String clasz : stack.getItem().getToolClasses(stack)) {
			System.err.printf("Class: %s\n", clasz);
		}

		temp.clear();

		Class itemClass = stack.getItem().getClass();

		for (Entry<Class, ArrayList<ItemStatTracker>> entry : statTrackersByClass.entrySet()) {
			if (entry.getKey().isAssignableFrom(itemClass)) {
				temp.addAll(entry.getValue());
			}
		}

		/*
		 * if (statTrackersByClass.containsKey(itemClass)) {
		 * temp.addAll(statTrackersByClass.get(itemClass)); }
		 */

		for (ItemStatTracker e : genericTrackers) {
			if (!temp.contains(e)) {
				for (Class c : e.classes) {
					if (c.isAssignableFrom(itemClass)) {
						ArrayList<ItemStatTracker> list = null;

						if (statTrackersByClass.containsKey(itemClass)) {
							list = new ArrayList<ItemStatTracker>();
							statTrackersByClass.put(itemClass, list);
						} else {
							list = statTrackersByClass.get(itemClass);
						}

						try {
							if (!statTrackersByClass.get(itemClass).contains(e)) {
								list.add(e);
							}

							temp.add(e);

							break;
						} catch (NullPointerException npex) {

						}
					}
				}
			}
		}

		if (!temp.isEmpty()) {
			if (!stack.hasTagCompound()) {
				stack.setTagCompound(new NBTTagCompound());
			}

			String processed = ItemStatHelper.getStat(stack, "processed");

			if (processed == null || processed.equals("false")) {
				stack.getTagCompound().setTag("Lore", new NBTTagList());

				ItemStatHelper.giveStat(stack, "processed", "true");

				for (ItemStatTracker statTracker : temp) {
					for (ItemStat s : statTracker.stats) {
						giveStat(stack, s.name, s.getNewValue(stack, r).toString());

						String lore = s.getLore(stack);
						if (lore != null) {
							addLore(stack, lore);
						}

						setName(stack, s.getAlteredStackName(stack, r));

						s.modifyStack(stack, r);
					}
				}
			}
		} else {
			skip.add(itemClass);
		}
	}

	@SubscribeEvent
	public void onCrafting(ItemCraftedEvent event) {

		EntityPlayer player = event.player;
		ItemStack itemStk = event.crafting;

		if (!player.worldObj.isRemote) {
			String itemName = null;

			Item item = itemStk.getItem();
			if (item == null)
				return;

			if (item instanceof ItemSword || item instanceof ItemBow || item instanceof ItemAxe) {
				itemName = EntityStatHelper.getUnalteredItemName(item);
			}

			if (ConfigData.enableCraftingStats && itemName != null) {
				if (!player.getEntityData().hasKey("CraftingStats")) {
					player.getEntityData().setTag("CraftingStats", new NBTTagCompound());
				}
				NBTTagCompound craftingStats = player.getEntityData().getCompoundTag("CraftingStats");

				if (!craftingStats.hasKey(itemName)) {
					craftingStats.setInteger(itemName, 0);

					if (!craftingStats.hasKey("TrackedItemList")) {
						craftingStats.setString("TrackedItemList", itemName);
					} else {
						craftingStats.setString("TrackedItemList",
								craftingStats.getString("TrackedItemList") + ";" + itemName);
					}
				}

				craftingStats.setInteger(itemName, craftingStats.getInteger(itemName) + 1);

				for (int i = 0; i < EntityStatHelper.knowledge.length; i++) {
					if (EntityStatHelper.killCount[i] == craftingStats.getInteger(itemName)) {
						NewPacketHandler.SEND_MESSAGE.sendToPlayer(player, "\u00A7o\u00A73You've become a "
								+ EntityStatHelper.knowledge[i].toLowerCase() + " " + itemName.toLowerCase()
								+ " smith! ("
								+ (i < EntityStatHelper.knowledge.length - 1
										? (EntityStatHelper.killCount[i + 1] * 2 - EntityStatHelper.killCount[i] * 2)
												+ " " + itemName.toLowerCase() + " crafts to next rank."
										: ""));
						break;
					}
				}
			}
		}
	}

	public void register() {
		MinecraftForge.EVENT_BUS.register(this);
	}
}
