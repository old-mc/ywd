package fyresmodjam.misc;

import net.minecraft.util.DamageSource;

public class DamageSources {
	private static boolean isInitted = false;

	public static DamageSource inferno;
	public static DamageSource retaliation;
	public static DamageSource trap;

	public static void doInit() {
		if (isInitted) {
			inferno = new DamageSource("inferno").setDamageBypassesArmor().setDamageIsAbsolute();

			retaliation = new DamageSource("retaliation").setDamageBypassesArmor();

			trap = new DamageSource("ywdtrap");
			
			isInitted = true;
		}
	}
}