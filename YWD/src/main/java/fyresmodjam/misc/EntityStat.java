package fyresmodjam.misc;

import java.util.Random;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;

public class EntityStat {
	public String name;
	public String value;

	public EntityStat(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public Object getNewValue(Random r) {
		return value;
	}

	public String getAlteredEntityName(EntityLiving entity) {
		return entity.getCommandSenderName();
	}

	public void modifyEntity(Entity entity) {
	}
}