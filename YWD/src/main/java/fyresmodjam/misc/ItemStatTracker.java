package fyresmodjam.misc;

import java.util.ArrayList;

public class ItemStatTracker {
	public Class<?>[] classes;

	public boolean instanceAllowed = false;

	public ItemStatTracker(Class<?>[] classes, boolean instanceAllowed) {
		this.classes = classes;
		this.instanceAllowed = instanceAllowed;
	}

	public ItemStatTracker(Class<?> c, boolean instanceAllowed) {
		this(new Class[] { c }, instanceAllowed);
	}

	public ArrayList<ItemStat> stats = new ArrayList<ItemStat>();

	public void addStat(ItemStat stat) {
		if (!stats.contains(stat)) {
			stats.add(stat);
		}
	}
}