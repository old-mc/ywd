package fyresmodjam.misc;

import org.lwjgl.input.Keyboard;

import net.minecraftforge.common.config.Configuration;

public class ConfigData {
	/*
	 * Categories for config options.
	 */
	private static final String CTG_WORLDGEN = "worldgen";
	private static final String CTG_MOBS = "mobs";

	/*
	 * Starting ID for achievements
	 */
	public static int achievementID = 2500;

	/*
	 * Keybindings
	 */
	public static int examineKey = Keyboard.KEY_X;
	public static int blessingKey = Keyboard.KEY_K;

	/*
	 * World generation settings
	 */

	// Pillars
	public static int pillarGenChance = 75;
	public static int maxPillarsPerChunk = 3;
	public static boolean spawnRandomPillars = true;
	
	// Towers
	public static int towerGenChance = 225;
	public static boolean spawnTowers = true;
	
	// Traps
	public static int trapGenChance = 300;
	public static boolean spawnTraps = true;
	public static boolean trapsBelowGroundOnly = false;
	
	// Mushrooms
	public static int mushroomReplaceChance = 15;

	/*
	 * Statistic tracking
	 */
	public static boolean enableWeaponKillStats = true;
	public static boolean enableMobKillStats = true;
	public static boolean enableCraftingStats = true;

	/*
	 * Blessing/Mark parameters
	 */
	public static final double RETALIATION_CHANCE = 0.05;

	/*
	 * Misc. Toggles
	 */
	public static boolean pillarGlow = true;
	public static boolean disableDisadvantages = false;
	public static boolean versionChecking = true;
	public static boolean showAllPillarsInCreative = false;

	/*
	 *  Mob config data
	 */
	// One in X normal mobs will receive a blessing
	public static int diabolicChance = 20;
	// A mob of at least this level will receive a blessing
	public static int blessingLevel = 5;
	// Determines how likely a mob is to be leveled up. Higher is lower
	public static int levelupChance = 3;
	// A mob gets this percentage of their health additional per level
	public static double healthLevelMult = 0.25;
	// A mob gets this flat amount on top of their adjusted health, per level
	public static int healthLevelFlat = 1;

	public static void loadFromConfig(Configuration config) {
		diabolicChance = config.get(CTG_MOBS, "diabolic_chance", diabolicChance).getInt();
		blessingLevel = config.get(CTG_MOBS, "blessing_level", diabolicChance).getInt();
		levelupChance = config.get(CTG_MOBS, "levelup_chance", diabolicChance).getInt();
		healthLevelMult = config.get(CTG_MOBS, "health_level_mult", 0).getDouble();
		healthLevelFlat = config.get(CTG_MOBS, "health_level_flat", 0).getInt();

		pillarGlow = config.get(Configuration.CATEGORY_GENERAL, "pillar_glow", pillarGlow).getBoolean(pillarGlow);

		pillarGenChance = config.get(CTG_WORLDGEN, "pillar_gen_difficulty", pillarGenChance).getInt();
		maxPillarsPerChunk = config.get(CTG_WORLDGEN, "max_pillars_per_chunk", maxPillarsPerChunk).getInt();
		towerGenChance = config.get(CTG_WORLDGEN, "tower_gen_difficulty", towerGenChance).getInt();
		trapGenChance = config.get(CTG_WORLDGEN, "trap_gen_difficulty", trapGenChance).getInt();
		mushroomReplaceChance = config.get(CTG_WORLDGEN, "mushroom_replace_difficulty", mushroomReplaceChance).getInt();
		spawnTraps = !(config.get(CTG_WORLDGEN, "disable_traps", !spawnTraps).getBoolean(!spawnTraps));
		spawnTowers = config.get(CTG_WORLDGEN, "spawn_towers", spawnTowers).getBoolean(spawnTowers);
		spawnRandomPillars = config.get(CTG_WORLDGEN, "spawn_random_pillars", spawnRandomPillars)
				.getBoolean(spawnRandomPillars);

		disableDisadvantages = config.get(Configuration.CATEGORY_GENERAL, "disable_disadvantages", disableDisadvantages)
				.getBoolean(disableDisadvantages);
		versionChecking = config.get(Configuration.CATEGORY_GENERAL, "version_checking", versionChecking)
				.getBoolean(versionChecking);

		showAllPillarsInCreative = config
				.get(Configuration.CATEGORY_GENERAL, "show_all_pillars_in_creative", showAllPillarsInCreative)
				.getBoolean(showAllPillarsInCreative);

		enableMobKillStats = config.get(Configuration.CATEGORY_GENERAL, "enable_mob_kill_stats", enableMobKillStats)
				.getBoolean(enableMobKillStats);
		enableWeaponKillStats = config
				.get(Configuration.CATEGORY_GENERAL, "enable_weapon_kill_stats", enableWeaponKillStats)
				.getBoolean(enableWeaponKillStats);
		enableCraftingStats = config.get(Configuration.CATEGORY_GENERAL, "enable_crafting_stats", enableCraftingStats)
				.getBoolean(enableCraftingStats);

		trapsBelowGroundOnly = config
				.get(Configuration.CATEGORY_GENERAL, "traps_below_ground_only", trapsBelowGroundOnly)
				.getBoolean(trapsBelowGroundOnly);
	}
}