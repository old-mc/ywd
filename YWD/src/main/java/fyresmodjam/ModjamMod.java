package fyresmodjam;

import java.io.File;
import java.util.EnumMap;
import java.util.Random;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.eventhandler.EventBus;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerChangedDimensionEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import cpw.mods.fml.common.network.FMLEmbeddedChannel;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import fyresmodjam.blessings.Blessing;
import fyresmodjam.blessings.BlessingUtils;
import fyresmodjam.blocks.BlockMysteryMushroom;
import fyresmodjam.blocks.BlockPillar;
import fyresmodjam.blocks.BlockTrap;
import fyresmodjam.commands.*;
import fyresmodjam.entities.EntityMysteryPotion;
import fyresmodjam.handlers.ChannelHandler;
import fyresmodjam.handlers.CommonTickHandler;
import fyresmodjam.handlers.GUIHandler;
import fyresmodjam.handlers.NewPacketHandler;
import fyresmodjam.items.*;
import fyresmodjam.misc.*;
import fyresmodjam.tileentities.TileEntityPillar;
import fyresmodjam.tileentities.TileEntityTrap;
import fyresmodjam.worldgen.FyresWorldData;
import fyresmodjam.worldgen.PillarGen;
import fyresmodjam.worldgen.WorldGenMoreDungeons;
import fyresmodjam.worldgen.WorldGenTrapsTowersAndMore;
import net.minecraft.block.Block;
import net.minecraft.command.CommandHandler;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.*;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.stats.Achievement;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraftforge.common.AchievementPage;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.entity.player.PlayerEvent;

@Mod(modid = "fyresmodjam", name = "Fyres ModJam Mod", version = "0.0.3d")
public class ModjamMod extends CommandHandler {
	@SidedProxy(clientSide = "fyresmodjam.ClientProxy", serverSide = "fyresmodjam.CommonProxy")
	public static CommonProxy proxy;

	@Instance("fyresmodjam")
	public static ModjamMod instance;

	public static EnumMap<Side, FMLEmbeddedChannel> channels = NetworkRegistry.INSTANCE.newChannel("YWDMod",
			new ChannelHandler());

	public static Random r = new Random();

	public static CreativeTabs tabModjamMod = new CreativeTabModjamMod(CreativeTabs.getNextID(),
			"The \"You Will Die\" Mod");

	public static Block blockPillar;
	public static Block blockTrap;
	public static Block mysteryMushroomBlock;

	public static Item itemPillar;
	public static Item mysteryPotion;
	public static Item itemTrap;
	public static Item mysteryMushroom;
	public static Item sceptre;

	public static Item crystalItem;
	public static Item scroll;

	public static Block crystal;
	public static Block crystalStand;

	public static Achievement startTheGame;
	public static Achievement losingIsFun;
	public static Achievement whoops;

	public static Achievement theHunt;
	public static Achievement jackOfAllTrades;

	public static AchievementPage page;

	public static String version = "v0.0.3e";
	public static String foundVersion = "v0.0.3e";
	public static boolean newerVersion = false;

	public static String configPath = null;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		String absolutePath = event.getSuggestedConfigurationFile().getAbsolutePath();

		File old = new File(absolutePath.replace("fyresmodjam", "YouWillDieMod"));

		if (old.exists()) {
			old.delete();
			System.out.println(true);
		}

		configPath = absolutePath.replace("fyresmodjam", "TheYouWillDieMod");

		Configuration config = new Configuration(new File(configPath));

		config.load();

		ConfigData.loadFromConfig(config);
		BlessingUtils.loadBlessings(config);

		config.save();

		// Item and Block loading

		blockPillar = new BlockPillar().setBlockUnbreakable().setResistance(6000000.0F);
		blockTrap = new BlockTrap().setBlockUnbreakable().setResistance(6000000.0F);
		mysteryMushroomBlock = new BlockMysteryMushroom().setHardness(0.0F).setStepSound(Block.soundTypeGrass)
				.setLightLevel(0.125F).setBlockName("mysteryMushroomBlock");

		itemPillar = new ItemPillar().setUnlocalizedName("blockPillar");
		GameRegistry.registerItem(itemPillar, "itemPillar");

		mysteryPotion = new ItemMysteryPotion().setUnlocalizedName("mysteryPotion")
				.setCreativeTab(CreativeTabs.tabBrewing);
		GameRegistry.registerItem(mysteryPotion, "mysteryPotion");

		itemTrap = new ItemTrap().setUnlocalizedName("itemTrap").setCreativeTab(CreativeTabs.tabBlock);
		GameRegistry.registerItem(itemTrap, "itemTrap");

		mysteryMushroom = new ItemMysteryMushroom().setUnlocalizedName("mysteryMushroom")
				.setCreativeTab(CreativeTabs.tabBrewing);
		GameRegistry.registerItem(mysteryMushroom, "mysteryMushroom");

		sceptre = new ItemObsidianSceptre().setUnlocalizedName("sceptre").setCreativeTab(CreativeTabs.tabTools)
				.setFull3D();
		GameRegistry.registerItem(sceptre, "sceptre");

		GameRegistry.registerBlock(blockPillar, "blockPillar");
		GameRegistry.registerTileEntity(TileEntityPillar.class, "Pillar Tile Entity");

		GameRegistry.registerBlock(blockTrap, "blockTrap");
		GameRegistry.registerTileEntity(TileEntityTrap.class, "Trap Entity");

		GameRegistry.registerBlock(mysteryMushroomBlock, "mysteryMushroomBlock");

		startTheGame = getNewAchievement(ConfigData.achievementID, 0, 0, new ItemStack(Items.iron_sword, 1),
				"startTheGame", "You Will Die", "Join a world with this mod installed", null, true);
		losingIsFun = getNewAchievement(ConfigData.achievementID + 1, -2, 0, new ItemStack(itemTrap, 1), "losingIsFun",
				"Losing Is Fun", "Experience \"fun\"", startTheGame, false);
		whoops = getNewAchievement(ConfigData.achievementID + 2, 2, 0, new ItemStack(itemTrap, 1, 1), "whoops",
				"Whoops", "Fail to disarm a trap", startTheGame, false);
		theHunt = getNewAchievement(ConfigData.achievementID + 3, 0, -2, new ItemStack(Items.bow, 1), "theHunt",
				"The Hunt", "Become a competent slayer of 5 or more different creatures", startTheGame, false);
		jackOfAllTrades = getNewAchievement(ConfigData.achievementID + 4, 0, 2, new ItemStack(Blocks.crafting_table, 1),
				"jackOfAllTrades", "Jack of All Trades", "Become a novice user of at least 10 different weapons",
				startTheGame, false);

		page = new AchievementPage("The \"You Will Die\" Mod", startTheGame, losingIsFun, whoops, theHunt,
				jackOfAllTrades);

		AchievementPage.registerAchievementPage(page);
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {

		// Registering
		EventBus bus = FMLCommonHandler.instance().bus();

		CommonTickHandler commonHandler = new CommonTickHandler();
		bus.register(commonHandler);

		MinecraftForge.EVENT_BUS.register(this);
		bus.register(this);

		new ItemStatHelper().register();
		new EntityStatHelper().register();

		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GUIHandler());

		GameRegistry.registerWorldGenerator(new PillarGen(), 0);

		GameRegistry.registerWorldGenerator(new WorldGenTrapsTowersAndMore(), 0);

		for (int i = 0; i < 3; i++) {
			GameRegistry.registerWorldGenerator(new WorldGenMoreDungeons(), 0);
		}

		EntityRegistry.registerGlobalEntityID(EntityMysteryPotion.class, "MysteryPotion",
				EntityRegistry.findGlobalUniqueEntityId());
		EntityRegistry.registerModEntity(EntityMysteryPotion.class, "MysteryPotion", 0, instance, 128, 1, true);

		GameRegistry.addShapelessRecipe(new ItemStack(itemTrap, 1, 0),
				new Object[] { Blocks.heavy_weighted_pressure_plate, Blocks.cactus });
		GameRegistry.addShapelessRecipe(new ItemStack(itemTrap, 1, 1),
				new Object[] { Blocks.heavy_weighted_pressure_plate, Blocks.torch });
		GameRegistry.addShapelessRecipe(new ItemStack(itemTrap, 1, 2),
				new Object[] { Blocks.heavy_weighted_pressure_plate, new ItemStack(Items.dye, 1, 0) });

		for (int i = 0; i < 13; i++) {
			GameRegistry.addShapelessRecipe(new ItemStack(mysteryPotion, 1, i + 13),
					new Object[] { new ItemStack(mysteryPotion, 1, i), Items.gunpowder });
			GameRegistry.addShapelessRecipe(new ItemStack(mysteryPotion, 1, i), new Object[] {
					new ItemStack(Items.potionitem, 1, 0), Items.leather, new ItemStack(mysteryMushroom, 1, i) });
		}

		// GameRegistry.addRecipe(new ItemStack(sceptre, 1, 0), "X", "Y", "X", 'X',
		// Blocks.obsidian, 'Y',
		// Blocks.end_stone);
		// GameRegistry.addShapelessRecipe(new ItemStack(sceptre, 1, 1),
		// new Object[] { new ItemStack(sceptre, 1, 0), Items.ender_pearl, Items.book
		// });

		proxy.register();

		// Entity Trackers
		EntityStatTracker playerTracker = new EntityStatTracker(EntityPlayer.class, true);

		playerTracker.addStat(new EntityStat("BlessingCooldown", "" + 0));
		playerTracker.addStat(new EntityStat("BlessingCounter", "" + 0));

		EntityStatTracker mobTracker = new EntityStatTracker(EntityMob.class, true);

		mobTracker.addStat(new EntityLevelStat("Level", ""));

		EntityStatHelper.addStatTracker(mobTracker);

		// Item Trackers
		ItemStatTracker weaponTracker = new ItemStatTracker(
				new Class[] { ItemSword.class, ItemAxe.class, ItemBow.class }, true);

		weaponTracker.addStat(new WeaponRankStat("Rank", ""));

		ItemStatHelper.addStatTracker(weaponTracker);

		ItemStatTracker armorTracker = new ItemStatTracker(new Class[] { ItemArmor.class }, true);

		armorTracker.addStat(new ArmorStatTracker("Rank", ""));

		ItemStatHelper.addStatTracker(armorTracker);

		// Other

		for (int i = 0; i < 13; i++) {
			ChestGenHooks.getInfo(ChestGenHooks.DUNGEON_CHEST)
					.addItem(new WeightedRandomChestContent(mysteryPotion, i, 1, 3, 2));

			WorldGenTrapsTowersAndMore.chestGenInfo.addItem(new WeightedRandomChestContent(mysteryPotion, i, 1, 3, 2));
		}
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {

	}

	@SubscribeEvent
	public void onPlayerLogin(PlayerLoggedInEvent event) {
		EntityPlayer player = event.player;

		if (!player.worldObj.isRemote) {
			NewPacketHandler.UPDATE_WORLD_DATA.sendToPlayer(player, CommonTickHandler.worldData.potionValues,
					CommonTickHandler.worldData.potionDurations, CommonTickHandler.worldData.getDisadvantage(),
					CommonTickHandler.worldData.currentTask, CommonTickHandler.worldData.currentTaskID,
					CommonTickHandler.worldData.currentTaskAmount, CommonTickHandler.worldData.progress,
					CommonTickHandler.worldData.tasksCompleted, CommonTickHandler.worldData.enderDragonKilled,
					ConfigData.spawnTraps, CommonTickHandler.worldData.rewardLevels,
					CommonTickHandler.worldData.mushroomColors);

			// TODO make tasks into their own class
			String name = CommonTickHandler.worldData.currentTask.equals("Kill")
					? FyresWorldData.validMobNames[CommonTickHandler.worldData.currentTaskID]
					: FyresWorldData.validItems[CommonTickHandler.worldData.currentTaskID].getDisplayName();

			if (CommonTickHandler.worldData.currentTaskAmount > 1) {
				if (name.contains("Block")) {
					name = name.replace("Block", "Blocks").replace("block", "blocks");
				} else {
					name += "s";
				}
			}

			// TODO move disadvantages into a class
			int index = -1;
			for (int i = 0; i < FyresWorldData.validDisadvantages.length; i++) {
				if (FyresWorldData.validDisadvantages[i].equals(CommonTickHandler.worldData.getDisadvantage())) {
					index = i;
					break;
				}
			}

			NewPacketHandler.SEND_MESSAGE.sendToPlayer(player,
					"\u00A7eWorld disadvantage: " + CommonTickHandler.worldData.getDisadvantage()
							+ (index == -1 ? "" : " (" + FyresWorldData.disadvantageDescriptions[index] + ")"));

			NewPacketHandler.SEND_MESSAGE.sendToPlayer(player,
					"\u00A7eWorld goal: " + CommonTickHandler.worldData.currentTask + " "
							+ CommonTickHandler.worldData.currentTaskAmount + " " + name + ". ("
							+ CommonTickHandler.worldData.progress + " " + CommonTickHandler.worldData.currentTask
							+ "ed)");

			NBTTagCompound playerData = player.getEntityData();
			if (!BlessingUtils.hasBlessing(player)) {
				String selectedBlessing = BlessingUtils.getPlayerBlessing();
				Blessing blessing = BlessingUtils.getBlessingInstance(selectedBlessing);

				playerData.setString("Blessing", selectedBlessing);

				NewPacketHandler.SEND_MESSAGE.sendToPlayer(player, "\u00A72You've been granted the "
						+ blessing.customName() + ". (Use /currentBlessing to check effect)");
			}

			NewPacketHandler.UPDATE_BLESSING.sendToPlayer(player, playerData.getString("Blessing"));

			if (EntityStatHelper.hasStat(player, "BlessingCounter")) {
				NewPacketHandler.UPDATE_STAT.sendToPlayer(player, "BlessingCounter",
						EntityStatHelper.getStat(player, "BlessingCounter"));
			}

			if (!playerData.hasKey("PotionKnowledge")) {
				int[] potionData = new int[13];

				for (int i = 0; i < 13; i++) {
					potionData[i] = -1;
				}

				playerData.setIntArray("PotionKnowledge", potionData);
			}

			NewPacketHandler.UPDATE_POTION_KNOWLEDGE.sendToPlayer(player, playerData.getIntArray("PotionKnowledge"));
		}

		if (ConfigData.versionChecking && newerVersion) {
			player.addChatComponentMessage(new ChatComponentTranslation("fyresmodjam.newVersion"));
		}

		player.triggerAchievement(startTheGame);
	}

	@SubscribeEvent
	public void onPlayerChangedDimension(PlayerChangedDimensionEvent event) {
		EntityPlayer player = event.player;

		NewPacketHandler.UPDATE_WORLD_DATA.sendToPlayer(player, CommonTickHandler.worldData.potionValues,
				CommonTickHandler.worldData.potionDurations, CommonTickHandler.worldData.getDisadvantage(),
				CommonTickHandler.worldData.currentTask, CommonTickHandler.worldData.currentTaskID,
				CommonTickHandler.worldData.currentTaskAmount, CommonTickHandler.worldData.progress,
				CommonTickHandler.worldData.tasksCompleted, CommonTickHandler.worldData.enderDragonKilled,
				ConfigData.spawnTraps, CommonTickHandler.worldData.rewardLevels,
				CommonTickHandler.worldData.mushroomColors);

		NewPacketHandler.UPDATE_BLESSING.sendToPlayer(player, BlessingUtils.getBlessing(player));

		if (EntityStatHelper.hasStat(player, "BlessingCounter")) {
			NewPacketHandler.UPDATE_STAT.sendToPlayer(player, "BlessingCounter",
					EntityStatHelper.getStat(player, "BlessingCounter"));
		}

		if (!player.getEntityData().hasKey("PotionKnowledge")) {
			int[] potionData = new int[13];

			for (int i = 0; i < 13; i++) {
				potionData[i] = -1;
			}

			player.getEntityData().setIntArray("PotionKnowledge", potionData);
		}

		NewPacketHandler.UPDATE_POTION_KNOWLEDGE.sendToPlayer(player,
				player.getEntityData().getIntArray("PotionKnowledge"));
	}

	@SubscribeEvent
	public void checkBreakSpeed(PlayerEvent.BreakSpeed event) {
		EntityPlayer entityPlayer = event.entityPlayer;

		if (entityPlayer != null && BlessingUtils.hasBlessing(entityPlayer)) {
			BlessingUtils.getBlessingInstance(entityPlayer).checkBreakSpeed(event);
		}
	}

	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) {
		initCommands(event);
	}

	public void initCommands(FMLServerStartingEvent event) {
		event.registerServerCommand(new CommandCurrentBlessing());
		event.registerServerCommand(new CommandCurrentDisadvantage());
		event.registerServerCommand(new CommandCurrentWorldTask());
		event.registerServerCommand(new CommandKillStats());
		event.registerServerCommand(new CommandWeaponStats());
		event.registerServerCommand(new CommandCraftingStats());
	}

	@SuppressWarnings("deprecation")
	public static Achievement getNewAchievement(int id, int x, int y, ItemStack stack, String name, String displayName,
			String desc, Achievement prereq, boolean independent) {
		Achievement achievement = new Achievement("YWD-" + id, name, x, y, stack, prereq);
		if (independent) {
			achievement = achievement.initIndependentStat();
		}
		LanguageRegistry.instance().addStringLocalization("achievement." + name, "en_US", displayName);
		LanguageRegistry.instance().addStringLocalization("achievement." + name + ".desc", "en_US", desc);
		achievement.registerStat();
		return achievement;
	}
}