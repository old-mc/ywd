package fyresmodjam.blessings;

/**
 * Represents the different types of blessings.
 * 
 * @author bjculkin
 */
public enum BlessingType {
	/**
	 * A standard blessing.
	 * 
	 * Normal scale benefit.
	 */
	STANDARD("Blessing", "Blessing of the"),

	/**
	 * A blessing with a down-side.
	 * 
	 * Stronger benefit, weak down-side.
	 */
	MARK("Mark", "Mark of");

	public final String internalPrefix;
	public final String displayPrefix;

	private BlessingType(String internalPrefix, String displayPrefix) {
		this.internalPrefix = internalPrefix;
		this.displayPrefix = displayPrefix;
	}
}