package fyresmodjam.blessings.marks;

public class ScoutingMark extends Mark {
	public ScoutingMark() {
		super("Scouting", false);
	}

	@Override
	public String benefit() {
		return "You can see traps without sneaking";
	}

	@Override
	public String drawback() {
		return "Take increased damage from traps";
	}
}
