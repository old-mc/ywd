package fyresmodjam.blessings.marks;

import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class FalseLifeMark extends Mark {
	public FalseLifeMark() {
		super("False Life", false);
	}

	@Override
	public String benefit() {
		return "Rally from a fatal injury";
	}

	@Override
	public String drawback() {
		return "Lose permanent health to do so";
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent event, float damageMultiplier) {
		float totalAmount = Math.max(0.5f, event.ammount * damageMultiplier);

		if (totalAmount > event.entityLiving.getMaxHealth()) {
			event.setCanceled(true);

			event.entityLiving.getEntityAttribute(SharedMonsterAttributes.maxHealth)
					.setBaseValue(event.entityLiving.getMaxHealth() / 0.75f);

			event.entityLiving.setHealth(event.entityLiving.getMaxHealth());

			event.entityLiving.addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 30, 1));
			event.entityLiving.addPotionEffect(new PotionEffect(Potion.resistance.id, 20, 1));
			event.entityLiving.addPotionEffect(new PotionEffect(Potion.damageBoost.id, 20, 1));

			return 0;
		}

		return damageMultiplier;
	}
}
