package fyresmodjam.blessings.marks;

import net.minecraft.entity.Entity;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class HuntingMark extends Mark {
	public HuntingMark() {
		super("Hunting");
	}

	@Override
	public String benefit() {
		return "Deal greatly increased damage with projectile weapons";
	}

	@Override
	public String drawback() {
		return "Deal less damage from all other sources";
	}

	@Override
	public void correctBlessing(Entity ent) {
		if (!(ent instanceof IRangedAttackMob)) {
			ent.getEntityData().setString("Blessing", "MarkWar");
		}
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.source.isProjectile()) {
			return damageMultiplier + 0.4f;
		}

		return damageMultiplier - 0.2f;
	}

}
