package fyresmodjam.blessings.marks;

import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class VampirismMark extends Mark {
	public VampirismMark() {
		super("Vampirism", false);
	}

	@Override
	public String benefit() {
		return "Heal a substantial amount of the damage done to enemies";
	}

	@Override
	public String drawback() {
		return "Greatly weakened during the day and ignited in direct sunlight";
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent event, float damageMultiplier) {
		Entity targetEntity = event.entity;

		if (targetEntity.getBrightness(1.0F) > 0.5F && canSeeTheWorld(targetEntity.worldObj, targetEntity)) {
			return damageMultiplier + 0.4F;
		}

		return damageMultiplier;
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		Entity sourceEntity = event.source.getEntity();

		if (sourceEntity instanceof EntityLivingBase) {
			((EntityLivingBase) sourceEntity).heal((event.ammount * damageMultiplier) * 0.25F);

			boolean seeTheSky = canSeeTheWorld(sourceEntity.worldObj, sourceEntity);

			if (sourceEntity.getBrightness(1.0F) > 0.5F && seeTheSky) {
				return damageMultiplier - 0.4F;
			}
		}

		return damageMultiplier;
	}

	@Override
	public void commonTick(ServerTickEvent stev, EntityPlayer play) {
		if (play.getBrightness(1.0f) > 0.5f && canSeeTheWorld(play.worldObj, play) && play.ticksExisted % 20 == 0) {
			play.setFire(5);
		}
	}

	private boolean canSeeTheWorld(World wld, Entity ent) {
		return wld.canBlockSeeTheSky((int) (ent.posX), (int) (ent.posY), (int) (ent.posZ));
	}
}
