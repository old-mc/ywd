package fyresmodjam.blessings.marks;

import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class ArcaneNullityMark extends Mark {
	public ArcaneNullityMark() {
		super("Arcane Nullity");
	}

	@Override
	public String benefit() {
		return "Take no damage from magic";
	}

	@Override
	public String drawback() {
		return "Deal no damage with magic";
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent lhev, float damageMultiplier) {
		if (lhev.source.isMagicDamage()) {
			lhev.setCanceled(true);
		}

		return damageMultiplier;
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent lhev, float damageMultiplier) {
		if (lhev.source.isMagicDamage()) {
			lhev.setCanceled(true);
		}

		return damageMultiplier;
	}
}
