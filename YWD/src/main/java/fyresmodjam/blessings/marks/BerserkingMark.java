package fyresmodjam.blessings.marks;

import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class BerserkingMark extends Mark {
	public BerserkingMark() {
		super("Berserking");
	}

	@Override
	public String benefit() {
		return "Deal greatly increased damage";
	}

	@Override
	public String drawback() {
		return "Take greatly increased damage";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		return damageMultiplier + .5f;
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent event, float damageMultiplier) {
		return damageMultiplier + .5f;
	}
}
