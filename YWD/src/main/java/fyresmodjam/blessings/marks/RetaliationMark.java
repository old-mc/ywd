package fyresmodjam.blessings.marks;

import fyresmodjam.ModjamMod;
import fyresmodjam.misc.ConfigData;
import fyresmodjam.misc.DamageSources;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class RetaliationMark extends Mark {
	public RetaliationMark() {
		super("Retaliation");
	}

	@Override
	public String benefit() {
		return "Have a chance to reflect attacks";
	}

	@Override
	public String drawback() {
		return "Have a chance for your attacks to be reflected";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		doRetaliation(event, damageMultiplier);

		return damageMultiplier;
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent event, float damageMultiplier) {
		doRetaliation(event, damageMultiplier);

		return damageMultiplier;
	}

	private void doRetaliation(LivingHurtEvent event, float damageMultiplier) {
		if (!event.source.damageType.equals("retaliation")) {
			// Don't retaliate for retaliation
			if (ModjamMod.r.nextDouble() < ConfigData.RETALIATION_CHANCE) {
				event.entityLiving.attackEntityFrom(DamageSources.retaliation,
						Math.max(0.5f, event.ammount * damageMultiplier));

				event.setCanceled(true);
			}
		}
	}
}
