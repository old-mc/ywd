package fyresmodjam.blessings.marks;

import fyresmodjam.blessings.Blessing;
import fyresmodjam.blessings.BlessingType;

public abstract class Mark extends Blessing {
	protected Mark(String name) {
		this(name, true);
	}

	protected Mark(String name, boolean mobAppropriate) {
		super(name, mobAppropriate);
	}

	public abstract String benefit();

	public abstract String drawback();

	@Override
	public String description() {
		return String.format("BENEFIT - %s\nDRAWBACK - %s", benefit(), drawback());
	}

	@Override
	public BlessingType type() {
		return BlessingType.MARK;
	}
}
