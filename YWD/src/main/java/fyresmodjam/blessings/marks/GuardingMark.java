package fyresmodjam.blessings.marks;

import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class GuardingMark extends Mark {
	public GuardingMark() {
		super("Guarding");
	}

	@Override
	public String benefit() {
		return "Take less damage";
	}

	@Override
	public String drawback() {
		return "Deal less damage";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		return damageMultiplier - 1f;
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent lhev, float damageMultiplier) {
		return damageMultiplier - 1f;
	}
}
