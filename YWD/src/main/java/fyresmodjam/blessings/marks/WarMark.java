package fyresmodjam.blessings.marks;

import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class WarMark extends Mark {
	public WarMark() {
		super("War");
	}

	@Override
	public String benefit() {
		return "Deal greatly increased damage with melee weapons";
	}

	@Override
	public String drawback() {
		return "Deal less damage from all other sources";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if ((event.source.getDamageType().equals("player") || event.source.getDamageType().equals("mob"))) {
			return damageMultiplier + 0.4f;
		}

		return damageMultiplier - 0.2f;
	}
}
