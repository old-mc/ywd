package fyresmodjam.blessings.marks;

import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class HolyLightMark extends Mark {
	public HolyLightMark() {
		super("Holy Light", false);
	}

	@Override
	public String benefit() {
		return "Deal greatly increased damage to undead";
	}

	@Override
	public String drawback() {
		return "Heal non-undead for a small amount of the damage you deal to them";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.entityLiving.isEntityUndead()) {
			return damageMultiplier + .5f;
		}

		float healed = Math.max(0.5f, (event.ammount * damageMultiplier) * 0.25f);

		event.entityLiving.heal(healed);

		return damageMultiplier;
	}
}
