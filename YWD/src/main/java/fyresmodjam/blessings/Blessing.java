package fyresmodjam.blessings;

import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;

/**
 * Represents a blessing bestowed to a player.
 * 
 * @author bjculkin
 */
public abstract class Blessing {
	private static int nextOrdinal = 0;

	public final int ordinal;

	public final String shortName;

	protected Blessing(String name) {
		this(name, true);
	}

	protected Blessing(String name, boolean mobAppropriate) {
		ordinal = nextOrdinal++;

		shortName = name;

		BlessingUtils.playerBlessings.put(name, this);
		if (mobAppropriate) {
			BlessingUtils.mobBlessings.put(name, this);
		}
	}

	public boolean hasCustomName() {
		return true;
	}

	public String customName() {
		return type().displayPrefix + " " + name();
	}

	/**
	 * Get the type of this blessing.
	 * 
	 * @return The type of the blessing
	 */
	public abstract BlessingType type();

	/**
	 * Get the name of this blessing.
	 * 
	 * @return The name of this blessing.
	 */
	public String name() {
		return shortName;
	}

	/**
	 * Get the description of this blessing.
	 * 
	 * @return The description of this blessing.
	 */
	public abstract String description();

	/**
	 * Modify the player's break speed.
	 * 
	 * @param pebsev
	 *            The break speed event.
	 */
	public void checkBreakSpeed(PlayerEvent.BreakSpeed pebsev) {
		return;
	}

	/**
	 * Correct inappropriate blessings for mobs.
	 * 
	 * @param ent
	 *            The mob to correct for.
	 */
	public void correctBlessing(Entity ent) {
		return;
	}

	/**
	 * Do something on every tick.
	 * 
	 * @param stev
	 *            The tick event
	 */
	public void commonTick(ServerTickEvent stev, EntityPlayer play) {
		return;
	}

	/**
	 * Handle adjusting the damage multiplier for mob knowledge.
	 * 
	 * @param lhev
	 *            The event being dealt with.
	 * 
	 * @param pickedDamageBonus
	 *            The base damage bonus for mob knowledge.
	 * 
	 * @return The new damage bonus for mob knowledge
	 */
	public float adjustMobKnowledgeBonus(LivingHurtEvent lhev, float pickedDamageBonus) {
		return pickedDamageBonus;
	}

	/**
	 * Handle adjusting the damage multiplier for weapon knowledge.
	 * 
	 * @param lhev
	 *            The event being dealt with.
	 * 
	 * @param pickedDamageBonus
	 *            The base damage bonus for weapon knowledge.
	 * 
	 * @return The new damage bonus for weapon knowledge
	 */
	public float adjustWeaponKnowledgeBonus(LivingHurtEvent lhev, float pickedDamageBonus) {
		return pickedDamageBonus;
	}

	public float onIncomingDamage(LivingHurtEvent lhev, float damageMultiplier) {
		return damageMultiplier;
	}

	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		return damageMultiplier;
	}

	public void onMobKill(LivingDeathEvent event) {
		return;
	}

	public void onPlayerDrops(PlayerDropsEvent event) {
		
	}
}
