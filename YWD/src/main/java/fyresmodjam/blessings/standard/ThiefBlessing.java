package fyresmodjam.blessings.standard;

import fyresmodjam.ModjamMod;
import net.minecraft.init.Items;
import net.minecraftforge.event.entity.living.LivingDeathEvent;

public class ThiefBlessing extends StandardBlessing {
	public ThiefBlessing() {
		super("Thief", false);
	}

	@Override
	public String description() {
		return "Enemies have a chance to drop gold nuggets";
	}

	@Override
	public void onMobKill(LivingDeathEvent event) {
		if (ModjamMod.r.nextInt(20) == 0) {
			if (!event.entity.worldObj.isRemote) {
				event.entity.dropItem(Items.gold_nugget, 1);
			}
		}
	}
}
