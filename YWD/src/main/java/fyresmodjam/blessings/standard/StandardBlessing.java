package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import fyresmodjam.blessings.BlessingType;

public abstract class StandardBlessing extends Blessing {
	protected StandardBlessing(String name) {
		this(name, true);
	}

	protected StandardBlessing(String name, boolean mobAppropriate) {
		super(name, mobAppropriate);
	}

	@Override
	public BlessingType type() {
		return BlessingType.STANDARD;
	}
}
