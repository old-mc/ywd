package fyresmodjam.blessings.standard;

import net.minecraft.entity.Entity;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class WarriorBlessing extends StandardBlessing {
	public WarriorBlessing() {
		super("Warrior");
	}

	@Override
	public String description() {
		return "Increased melee damage";
	}

	@Override
	public void correctBlessing(Entity ent) {
		if (ent instanceof IRangedAttackMob) {
			ent.getEntityData().setString("Blessing", "BlessingHunter");
		}
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if ((event.source.getDamageType().equals("player") || event.source.getDamageType().equals("mob"))) {
			return damageMultiplier + 0.2f;
		}

		return damageMultiplier;
	}
}
