package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;

public class MechanicBlessing extends StandardBlessing {
	public MechanicBlessing() {
		super("Mechanic", false);
	}

	@Override
	public String name() {
		return "Mechanic";
	}

	@Override
	public String description() {
		return "@@§ePASSIVE - §oYou disarm traps more often and have an increased chance to salvage disarmed traps.\n@@§eACTIVE - §oOnce per day, you may disarm and salvage a trap for free";
	}

}
