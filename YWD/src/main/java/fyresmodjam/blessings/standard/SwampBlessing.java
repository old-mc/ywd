package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class SwampBlessing extends StandardBlessing {
	public SwampBlessing() {
		super("Swamp");
	}

	@Override
	public String description() {
		return "Attacks slow enemies";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.entityLiving != null) {
			event.entityLiving.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 100, 1, false));
		}

		return damageMultiplier;
	}
}
