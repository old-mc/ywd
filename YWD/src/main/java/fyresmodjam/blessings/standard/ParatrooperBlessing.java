package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class ParatrooperBlessing extends StandardBlessing {
	public ParatrooperBlessing() {
		super("Paratrooper");
	}

	@Override
	public String description() {
		return "Take no fall damage";
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.source.damageType.equals("fall")) {
			event.setCanceled(true);

			return 0;
		}

		return damageMultiplier;
	}
}
