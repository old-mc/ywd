package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import fyresmodjam.blessings.BlessingUtils;
import fyresmodjam.misc.EntityStatHelper;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class BerserkerBlessing extends StandardBlessing {
	public BerserkerBlessing() {
		super("Berserker", false);
	}

	@Override
	public String description() {
		return "@@§ePASSIVE - §oKills are added as berserk charges. (10 max)@@§eACTIVE - §oTurn on/off berserk mode. While berserk mode is active, you do increased damage, and expend a counter every 2 seconds";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (BlessingUtils.isBlessingActive(event.source.getEntity())) {
			return damageMultiplier + 0.3f;
		}

		return damageMultiplier;
	}

	@Override
	public void onMobKill(LivingDeathEvent event) {
		if (!EntityStatHelper.hasStat(event.source.getEntity(), "BlessingCounter")) {
			EntityStatHelper.giveStat(event.source.getEntity(), "BlessingCounter", 0);
		}

		EntityStatHelper.giveStat(event.source.getEntity(), "BlessingCounter", Math.min(10,
				Integer.parseInt(EntityStatHelper.getStat(event.source.getEntity(), "BlessingCounter")) + 1));
	}
}
