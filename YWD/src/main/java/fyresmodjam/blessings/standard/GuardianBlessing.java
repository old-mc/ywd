package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class GuardianBlessing extends StandardBlessing {
	public GuardianBlessing() {
		super("Guardian");
	}

	@Override
	public String name() {
		return "Guardian";
	}

	@Override
	public String description() {
		return "Reduce proportional damage taken";
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent lhev, float damageMultiplier) {
		return damageMultiplier - 0.2f;
	}
}
