package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class LonerBlessing extends StandardBlessing {
	public LonerBlessing() {
		super("Loner");
	}

	@Override
	public String description() {
		return "Do higher damage the lower your health is";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.source.getEntity() instanceof EntityLivingBase) {
			float scaleFactor = 1.0F - (((EntityLivingBase) event.source.getEntity()).getHealth()
					/ ((EntityLivingBase) event.source.getEntity()).getMaxHealth());

			return damageMultiplier + 0.35F * scaleFactor;
		}

		return damageMultiplier;
	}
}
