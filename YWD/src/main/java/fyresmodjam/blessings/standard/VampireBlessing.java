package fyresmodjam.blessings.standard;

import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class VampireBlessing extends StandardBlessing {
	public VampireBlessing() {
		super("Vampire");
	}

	@Override
	public String description() {
		return "Heal a small percentage of damage dealt to enemies";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.source.getEntity() instanceof EntityLivingBase) {
			((EntityLivingBase) event.source.getEntity()).heal((event.ammount * damageMultiplier) * 0.07F);
		}

		return damageMultiplier;
	}
}
