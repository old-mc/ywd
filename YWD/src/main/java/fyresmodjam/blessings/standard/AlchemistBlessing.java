package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;

public class AlchemistBlessing extends StandardBlessing {
	public AlchemistBlessing() {
		super("Alchemist", false);
	}

	@Override
	public String name() {
		return "Alchemist";
	}

	@Override
	public String description() {
		return "All mystery potions act like wildcard potions";
	}
}
