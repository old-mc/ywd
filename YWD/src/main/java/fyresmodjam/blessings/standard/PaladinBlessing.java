package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class PaladinBlessing extends StandardBlessing {
	public PaladinBlessing() {
		super("Paladin", false);
	}

	@Override
	public String description() {
		return "Deal bonus damage to undead";
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.entityLiving.isEntityUndead()) {
			return damageMultiplier + .2f;
		}

		return damageMultiplier;
	}
}
