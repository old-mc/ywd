package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;

public class MinerBlessing extends StandardBlessing {
	public MinerBlessing() {
		super("Miner", false);
	}

	@Override
	public String description() {
		return "Increased breaking speed on stone and iron blocks, and increased damage with pickaxes";
	}

	@Override
	public void checkBreakSpeed(BreakSpeed pebsev) {
		if (pebsev.block.getMaterial() == Material.rock || pebsev.block.getMaterial() == Material.iron) {
			pebsev.newSpeed = pebsev.originalSpeed * 1.25F;
		}
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		ItemStack held = null;

		if (event.source.getEntity() instanceof EntityLivingBase) {
			held = ((EntityLivingBase) event.source.getEntity()).getHeldItem();
		}

		if (held != null && itemIsPickaxe(held)) {
			return damageMultiplier + 0.2F;
		}

		return damageMultiplier;
	}

	private boolean itemIsPickaxe(ItemStack held) {
		if (held.getItem() instanceof ItemTool) {
			return ((ItemTool) held.getItem()).getToolClasses(held).contains("pickaxe");
		}

		return false;
	}
}
