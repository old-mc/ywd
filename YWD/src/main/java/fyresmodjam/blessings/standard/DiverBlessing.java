package fyresmodjam.blessings.standard;

import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import fyresmodjam.blessings.Blessing;
import net.minecraft.entity.player.EntityPlayer;

public class DiverBlessing extends StandardBlessing {
	public DiverBlessing() {
		super("Diver", false);
	}

	@Override
	public String description() {
		return "Breathe underwater";
	}

	@Override
	public void commonTick(ServerTickEvent stev, EntityPlayer play) {
		play.setAir(0);
	}
}
