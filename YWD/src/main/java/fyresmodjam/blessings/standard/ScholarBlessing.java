package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class ScholarBlessing extends StandardBlessing {
	public ScholarBlessing() {
		super("Scholar", false);
	}

	@Override
	public String description() {
		return "Do more damage:\n- With weapons you are familiar with\n-Against enemies you are familiar with";
	}

	@Override
	public float adjustMobKnowledgeBonus(LivingHurtEvent lhev, float pickedDamageBonus) {
		return pickedDamageBonus + (pickedDamageBonus * 0.5f);
	}

	@Override
	public float adjustWeaponKnowledgeBonus(LivingHurtEvent lhev, float pickedDamageBonus) {
		return pickedDamageBonus + (pickedDamageBonus * 0.5f);
	}
}
