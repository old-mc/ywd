package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class ThickSkinnedBlessing extends StandardBlessing {
	public ThickSkinnedBlessing() {
		super("Thick-Skinned");
	}

	@Override
	public String description() {
		return "Reduce damage taken by a flat amount";
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent lhev, float damageMultiplier) {
		lhev.ammount = Math.max(0.5f, lhev.ammount - 1f);

		return damageMultiplier;
	}
}
