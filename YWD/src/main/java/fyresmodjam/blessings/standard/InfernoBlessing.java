package fyresmodjam.blessings.standard;

import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import fyresmodjam.blessings.Blessing;
import fyresmodjam.misc.DamageSources;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

// TODO
// replace this with a blessing that nullifies fire, and a mark that
// does that plus the damage boost
public class InfernoBlessing extends StandardBlessing {
	public InfernoBlessing() {
		super("Inferno");
	}

	@Override
	public String name() {
		return "Inferno";
	}

	@Override
	public String description() {
		return "Benefit: Take no fire damage";

		// return "Benefit: Take no fire damage, and do increased damage while on
		// fire\nDrawback: Take damge while wet";
	}

	@Override
	public void commonTick(ServerTickEvent stev, EntityPlayer play) {
		if (play.isWet() && play.ticksExisted % 10 == 0) {
			play.attackEntityFrom(DamageSources.inferno, 1.0F);
		}
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.source.isFireDamage() || event.source.getDamageType().equals("inFire")
				|| event.source.getDamageType().equals("onFire") || event.source.getDamageType().equals("lava")) {
			event.setCanceled(true);

			return 0;
		}

		return damageMultiplier;
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.source.getEntity().isBurning()) {
			// return damageMultiplier + 0.35f;
		}

		return damageMultiplier;
	}
}
