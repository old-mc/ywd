package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import net.minecraft.entity.Entity;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class HunterBlessing extends StandardBlessing {
	public HunterBlessing() {
		super("Hunter");
	}

	@Override
	public String description() {
		return "Increased projectile damage";
	}

	@Override
	public void correctBlessing(Entity ent) {
		if (!(ent instanceof IRangedAttackMob)) {
			ent.getEntityData().setString("Blessing", "BlessingWarrior");
		}
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.source.isProjectile()) {
			return damageMultiplier + 0.2f;
		}

		return damageMultiplier;
	}
}
