package fyresmodjam.blessings.standard;

import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import fyresmodjam.blessings.Blessing;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class NinjaBlessing extends StandardBlessing {
	public NinjaBlessing() {
		super("Ninja", false);
	}

	@Override
	public String description() {
		return "- Go invisible while sneaking\n- Attacks on enemies with full health deal 2x damage";
	}

	@Override
	public void commonTick(ServerTickEvent stev, EntityPlayer player) {
		if (player.isSneaking()) {
			PotionEffect e = player.getActivePotionEffect(Potion.invisibility);

			if (e == null || player.getActivePotionEffect(Potion.invisibility).getDuration() < 10) {
				player.addPotionEffect(new PotionEffect(Potion.invisibility.id, 10, 1, false));
			}
		}
	}

	@Override
	public float onOutgoingDamage(LivingHurtEvent event, float damageMultiplier) {
		if (event.entityLiving != null && event.source.getEntity().isSneaking()
				&& event.entityLiving.getHealth() == event.entityLiving.getMaxHealth()) {
			return damageMultiplier + 1.0F;
		}

		return damageMultiplier;
	}
}
