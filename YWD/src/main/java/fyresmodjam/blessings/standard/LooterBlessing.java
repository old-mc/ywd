package fyresmodjam.blessings.standard;

import fyresmodjam.ModjamMod;
import fyresmodjam.blessings.Blessing;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.event.entity.living.LivingDeathEvent;

public class LooterBlessing extends StandardBlessing {
	public LooterBlessing() {
		super("Looter", false);
	}

	@Override
	public String description() {
		return "Enemies have a chance to drop dungeon loot";
	}

	@Override
	public void onMobKill(LivingDeathEvent event) {
		if (ModjamMod.r.nextInt(50) == 0) {
			if (!event.entity.worldObj.isRemote) {
				event.entity.dropItem(ChestGenHooks.getOneItem(ChestGenHooks.DUNGEON_CHEST, ModjamMod.r).getItem(), 1);
			}
		}
	}
}
