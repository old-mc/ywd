package fyresmodjam.blessings.standard;

import fyresmodjam.blessings.Blessing;
import fyresmodjam.misc.DamageSources;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;

public class PorcupineBlessing extends StandardBlessing {
	public PorcupineBlessing() {
		super("Porcupine");
	}

	@Override
	public String description() {
		return "Deal retaliation damage to melee attackers";
	}

	@Override
	public float onIncomingDamage(LivingHurtEvent event, float damageMultiplier) {
		DamageSource damageSource = event.source;

		Entity source = damageSource.getEntity();

		if (source != null && source instanceof EntityLivingBase && !damageSource.isProjectile()
				&& (damageSource.getDamageType().equals("mob") || damageSource.getDamageType().equals("player"))) {

			DamageSource damage = DamageSource.causeThornsDamage(source);

			((EntityLivingBase) source).attackEntityFrom(damage, event.ammount * 0.07F);
		}

		return damageMultiplier;
	}
}
