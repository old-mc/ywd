package fyresmodjam.blessings;

import java.util.HashMap;
import java.util.Map;

import fyresmodjam.ModjamMod;
import fyresmodjam.handlers.CommonTickHandler;
import fyresmodjam.misc.EntityStatHelper;

import fyresmodjam.blessings.marks.*;
import fyresmodjam.blessings.standard.*;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.config.Configuration;

public class BlessingUtils {
	public static Map<String, Blessing> playerBlessings;
	public static Map<String, Blessing> mobBlessings;

	public static void loadBlessings(Configuration cfg) {
		playerBlessings = new HashMap<String, Blessing>();
		mobBlessings = new HashMap<String, Blessing>();

		createStandardBlessings();
		createMarks();
	}

	private static void createMarks() {
		BerserkingMark berserkingMark = new BerserkingMark();
		FalseLifeMark falseLifeMark = new FalseLifeMark();
		GuardingMark guardingMark = new GuardingMark();
		HuntingMark huntingMark = new HuntingMark();
		ScoutingMark scoutingMark = new ScoutingMark();
		// VampirismMark vampirismMark = new VampirismMark();
		WarMark warMark = new WarMark();

		addOmniBlessing("MarkBerserking", berserkingMark);
		addOmniBlessing("MarkFalseLife", falseLifeMark);
		addOmniBlessing("MarkGuarding", guardingMark);
		addOmniBlessing("MarkHunting", huntingMark);
		// addOmniBlessing("MarkVampirism", vampirismMark);
		addOmniBlessing("MarkWar", warMark);

		playerBlessings.put("MarkScouting", scoutingMark);
	}

	private static void createStandardBlessings() {
		AlchemistBlessing alchemistBlessing = new AlchemistBlessing();
		BerserkerBlessing berserkerBlessing = new BerserkerBlessing();
		GuardianBlessing guardianBlessing = new GuardianBlessing();
		HunterBlessing hunterBlessing = new HunterBlessing();
		InfernoBlessing infernoBlessing = new InfernoBlessing();
		LonerBlessing lonerBlessing = new LonerBlessing();
		ParatrooperBlessing paratrooperBlessing = new ParatrooperBlessing();
		PorcupineBlessing porcupineBlessing = new PorcupineBlessing();
		SwampBlessing swampBlessing = new SwampBlessing();
		ThickSkinnedBlessing thickSkinnedBlessing = new ThickSkinnedBlessing();
		VampireBlessing vampireBlessing = new VampireBlessing();
		WarriorBlessing warriorBlessing = new WarriorBlessing();
		DiverBlessing diverBlessing = new DiverBlessing();
		LooterBlessing looterBlessing = new LooterBlessing();
		LumberjackBlessing lumberjackBlessing = new LumberjackBlessing();
		MechanicBlessing mechanicBlessing = new MechanicBlessing();
		MinerBlessing minerBlessing = new MinerBlessing();
		NinjaBlessing ninjaBlessing = new NinjaBlessing();
		ScholarBlessing scholarBlessing = new ScholarBlessing();
		ThiefBlessing thiefBlessing = new ThiefBlessing();

		addOmniBlessing("BlessingWarrior", warriorBlessing);
		addOmniBlessing("BlessingHunter", hunterBlessing);
		addOmniBlessing("BlessingSwamp", swampBlessing);
		addOmniBlessing("BlessingGuardian", guardianBlessing);
		addOmniBlessing("BlessingVampire", vampireBlessing);
		addOmniBlessing("BlessingInferno", infernoBlessing);
		addOmniBlessing("BlessingLoner", lonerBlessing);
		addOmniBlessing("BlessingParatrooper", paratrooperBlessing);
		addOmniBlessing("BlessingPorcupine", porcupineBlessing);
		addOmniBlessing("BlessingThick-Skinned", thickSkinnedBlessing);

		PaladinBlessing paladinBlessing = new PaladinBlessing();

		playerBlessings.put("BlessingAlchemist", alchemistBlessing);
		playerBlessings.put("BlessingBerserker", berserkerBlessing);
		playerBlessings.put("BlessingDiver", diverBlessing);
		playerBlessings.put("BlessingLooter", looterBlessing);
		playerBlessings.put("BlessingLumberjack", lumberjackBlessing);
		playerBlessings.put("BlessingMechanic", mechanicBlessing);
		playerBlessings.put("BlessingMiner", minerBlessing);
		playerBlessings.put("BlessingNinja", ninjaBlessing);
		playerBlessings.put("BlessingPaladin", paladinBlessing);
		playerBlessings.put("BlessingScholar", scholarBlessing);
		playerBlessings.put("BlessingThief", thiefBlessing);
	}

	private static void addOmniBlessing(String blessName, Blessing blessInst) {
		playerBlessings.put(blessName, blessInst);
		mobBlessings.put(blessName, blessInst);
	}

	public static boolean hasDisadvantage() {
		if (CommonTickHandler.worldData != null) {
			String currentDisadvantage = CommonTickHandler.worldData.getDisadvantage();

			return currentDisadvantage != null && !(currentDisadvantage.equals(""));
		}

		return false;
	}

	public static boolean hasDisadvantage(String disadvantage) {
		if (hasDisadvantage()) {
			return CommonTickHandler.worldData.getDisadvantage().equals(disadvantage);
		}

		return false;
	}

	public static String getDisadvantage() {
		return CommonTickHandler.worldData.currentDisadvantage;
	}

	public static String getBlessing(Entity ent) {
		return ent.getEntityData().getString("Blessing");
	}

	public static boolean hasBlessing(Entity ent) {
		NBTTagCompound entityData = ent.getEntityData();

		return entityData.hasKey("Blessing") && !(entityData.getString("Blessing").equals(""));
	}

	public static boolean hasBlessing(Entity ent, String blessing) {
		if (hasBlessing(ent)) {
			return ent.getEntityData().getString("Blessing").equals(blessing);
		}

		return false;
	}

	public static boolean hasBlessingCounter(Entity ent) {
		return EntityStatHelper.hasStat(ent, "BlessingCounter");
	}

	public static int getBlessingCounter(Entity ent) {
		return Integer.parseInt(EntityStatHelper.getStat(ent, "BlessingCounter"));
	}

	public static boolean isBlessingActive(Entity ent) {
		return ent.getEntityData().hasKey("BlessingActive") && ent.getEntityData().getBoolean("BlessingActive");
	}

	public static Blessing getBlessingInstance(String blessing) {
		if (playerBlessings.containsKey(blessing)) {
			return playerBlessings.get(blessing);
		} else {
			System.err.printf("ERROR: Blessing '%s' not found, using default blessing\n", blessing);
			return playerBlessings.get("BlessingGuardian");
		}
	}

	public static String getPlayerBlessing() {
		int n = ModjamMod.r.nextInt(playerBlessings.size());

		int i = 0;
		for (String blessing : playerBlessings.keySet()) {
			if (i >= n) {
				return blessing;
			}

			i += 1;
		}

		System.err.println("ERROR: Couldn't get blessing for player; using default.");
		return "BlessingGuardian";
	}

	public static String getMobBlessing() {
		int n = ModjamMod.r.nextInt(mobBlessings.size());

		int i = 0;
		for (String blessing : mobBlessings.keySet()) {
			if (i >= n) {
				return blessing;
			}

			i += 1;
		}

		System.err.println("ERROR: Couldn't get blessing for mob; using default.");
		return "BlessingGuardian";
	}

	public static Blessing getBlessingInstance(Entity entity) {
		return getBlessingInstance(getBlessing(entity));
	}
}
